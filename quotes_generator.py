"""
This module includes a class QuotesGenerator that helps in generating
random quotes from multiple apis
"""
import time
import random
import requests

class QuotesGenerator:
    """
    This class is created to generate random quotes from mutiple apis:
    apis :
          https://breakingbadquotes.xyz
          https://api.quotable.io
          https://forismatic.com
          https://dummyjson.com
    you can add as much providers as you need
    """
    def __init__(self) -> None:
        self.retry = 0
        self.max_retry = 5
        ##############################################
        # You can add as much providers as you want  #
        # Just create a new method and add it to the #
        # quotes provider below
        #############################################
        self.quotes_provider = [
                self.get_one_breakingbadquote,
                self.get_one_dummyquote,
                self.get_one_forismatic,
                self.get_one_quotable
                ]
    def get_response(self, url, return_json=True):
        """
        This method simple take a link as parameter, make a requests
        and return back the response [json/text] as needed.
        param url (str) : the url to be requested
        param return_json (bool) : return type True for json, False for text
                                   default True.
        """
        try:
            response = requests.get(url=url,timeout=10)
            if return_json:
                response_content = response.json()
            else:
                response_content = response.text
            self.retry = 0
        except:# pylint: disable=W0702
            if self.retry >= self.max_retry:
                self.retry = 0
                return {}
            self.retry += 1
            # wait a moment
            time.sleep(0.5)
            self.get_response(url)
        return response_content
    def get_one_breakingbadquote(self):
        """
        This method returns a single Quote selected randomly
        from the collection of dummyquote api
        """
        url = 'https://api.breakingbadquotes.xyz/v1/quotes/1'
        data = self.get_response(url)
        if data:
            quote = data[0]['quote']
            author = data[0]['author']
            return {'quote':quote,
                    'author':author,
                    'quote_format':'text',
                    'source':'https://breakingbadquotes.xyz',
                    'tags':['breaking bad']}
        return None
    def get_one_dummyquote(self):
        """
        This method returns a single Quote selected randomly
        from the collection of dummyquote api
        """
        url = 'https://dummyjson.com/quotes/random'
        data = self.get_response(url)
        if data:
            quote = data['quote']
            author = data['author']
            return {'quote':quote,
                    'author':author,
                    'quote_format':'text',
                    'source':'https://dummyjson.com',
                    'tags':[author.lower()]}
        return None
    def get_one_forismatic(self):
        """
        This method returns a single Quote selected randomly
        from the collection of forismatic api
        """
        url = 'https://api.forismatic.com/api/1.0/?method=getQuote&format=html&lang=en&jsonp=?'
        data = self.get_response(url, return_json=False)
        if data:
            return {'quote':data,
                    'author':'author',
                    'quote_format':'html',
                    'source':'https://forismatic.com',
                    'tags':['quotes']}
        return None
    def get_one_quotable(self):
        """
        This method returns a single Quote selected randomly
        from the collection of quotable api
        """
        url = 'https://api.quotable.io/random'
        data = self.get_response(url)
        if data:
            quote = data['content']
            author = data['author']
            tags = data['tags']
            return {'quote':quote,
                    'author':author,
                    'quote_format':'text',
                    'source':'https://api.quotable.io',
                    'tags':tags}
        return None
    def get_random_quote(self):
        """
        This method returns a single Quote selected randomly
        from one of the providers randomly as well.
        """
        provider_choice = random.choice(self.quotes_provider)
        quote_choice = provider_choice()
        return quote_choice
if __name__ == '__main__':
    pass
