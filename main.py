"""
This module is the main program to run to auto post random quotes
in the provided Tumblr account
"""
import time
from quotes_generator import QuotesGenerator
from tumblr_autopost import TumblrAutoPost

if __name__ == '__main__':
    CONSUMER_KEY = ''
    CONSUMER_SECRET = ''
    TOKEN =  ''
    TOKEN_SECRET = ''
    tumblr_auto_post_obj = TumblrAutoPost(CONSUMER_KEY,
                                          CONSUMER_SECRET,
                                          TOKEN,
                                          TOKEN_SECRET)
    for _ in range(4):
        quotes_generator = QuotesGenerator()
        random_quote = quotes_generator.get_random_quote()
        quote = random_quote['quote']
        quote_format = random_quote['quote_format']
        tags = random_quote['tags']
        source = random_quote['source']
        tumblr_auto_post_obj.create_post(quote=quote,
                                        quote_format=quote_format,
                                        tags=tags,
                                        source=source)
        time.sleep(2)
        print('sleep for a while...')
    